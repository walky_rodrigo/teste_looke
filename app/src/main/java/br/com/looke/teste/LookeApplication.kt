package br.com.looke.teste

import android.app.Application
import br.com.looke.teste.di.appModelModule
import br.com.looke.teste.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class LookeApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@LookeApplication)
            modules(arrayListOf(appModule, appModelModule))
        }
    }
}