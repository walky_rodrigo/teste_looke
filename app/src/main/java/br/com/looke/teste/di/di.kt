package br.com.looke.teste.di

import br.com.looke.teste.domain.api.LookeApi
import br.com.looke.teste.domain.remote.HttpClient
import br.com.looke.teste.domain.repository.LookeRepository
import br.com.looke.teste.domain.repository.LookeRepositoryImpl
import br.com.looke.teste.ui.fragments.first.FirstViewModel
import br.com.looke.teste.ui.fragments.fourth.FourthViewModel
import br.com.looke.teste.ui.fragments.second.SecondViewModel
import br.com.looke.teste.ui.fragments.third.ThirdViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    factory { HttpClient() }
    factory { get<HttpClient>().create(LookeApi::class.java) }
    factory<LookeRepository> { LookeRepositoryImpl(get()) }
}

val appModelModule = module {
    viewModel { FirstViewModel(get()) }
    viewModel { SecondViewModel() }
    viewModel { ThirdViewModel() }
    viewModel { FourthViewModel() }
}

