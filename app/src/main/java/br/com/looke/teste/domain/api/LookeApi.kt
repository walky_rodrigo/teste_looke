package br.com.looke.teste.domain.api

import br.com.looke.teste.domain.model.LookeResponse
import retrofit2.http.GET

interface LookeApi {

    @GET("teste.json")
    suspend fun getCartVtex(): List<LookeResponse>

}