package br.com.looke.teste.domain.model

data class CarModel(val name: String, val price: Double)