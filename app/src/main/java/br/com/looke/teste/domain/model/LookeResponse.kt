package br.com.looke.teste.domain.model

data class LookeResponse(
        val topping: List<ToppingResponse> = arrayListOf()
)