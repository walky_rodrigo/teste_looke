package br.com.looke.teste.domain.model

data class ToppingResponse(
        val id: String? = "",
        val type: String? = "")