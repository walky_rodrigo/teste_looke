package br.com.looke.teste.domain.remote

sealed class SafeResponse<out T> {
    data class Success<out T>(val value: T) : SafeResponse<T>()
    data class GenericError(val errorMessage: String) : SafeResponse<Nothing>()
}

suspend fun <T> safeRequest(request: suspend () -> T): SafeResponse<T> {
    return try {
        SafeResponse.Success(request())
    } catch (throwable: Throwable) {
        val msg = "Ops. Tivemos um problema aqui. Por favor, tente novamente."
        return SafeResponse.GenericError(msg)
    }
}