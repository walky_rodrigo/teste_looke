package br.com.looke.teste.domain.repository

import br.com.looke.teste.domain.model.LookeResponse

interface LookeRepository {
    suspend fun getToppings(): List<LookeResponse>
}