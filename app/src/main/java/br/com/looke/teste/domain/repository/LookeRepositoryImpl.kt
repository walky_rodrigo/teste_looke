package br.com.looke.teste.domain.repository

import br.com.looke.teste.domain.api.LookeApi
import br.com.looke.teste.domain.model.LookeResponse

class LookeRepositoryImpl(private val lookeApi: LookeApi): LookeRepository {
    override suspend fun getToppings(): List<LookeResponse> {
        return lookeApi.getCartVtex()
    }
}