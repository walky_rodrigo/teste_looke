package br.com.looke.teste.extensions

import android.widget.EditText

fun EditText.getTextInt(): Int {
    return this.text.toString().toIntOrNull() ?: 0
}