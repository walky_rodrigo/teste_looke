package br.com.looke.teste.extensions

import android.widget.TextView

fun TextView.setTextOrEmpty(text: String?) {
    this.text = text ?: ""
}