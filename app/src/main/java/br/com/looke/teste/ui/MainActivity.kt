package br.com.looke.teste.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.looke.teste.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}