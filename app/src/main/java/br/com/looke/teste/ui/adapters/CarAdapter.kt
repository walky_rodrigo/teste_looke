package br.com.looke.teste.ui.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import br.com.looke.teste.R
import br.com.looke.teste.domain.model.CarModel
import kotlinx.android.synthetic.main.item_car_view.view.*
import java.text.NumberFormat

class CarAdapter(private val mList: List<CarModel>): RecyclerView.Adapter<CarAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_car_view, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = mList[position]
        holder.setName(item.name)
        holder.setPrice(item.price)
        holder.priceColor(position)
    }

    override fun getItemCount() = mList.size

    class Holder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val textName = itemView.tv_car_name
        private val textPrice = itemView.tv_car_price

        fun setName(name: String) {
            textName.text = name
        }

        fun setPrice(price: Double) {
            textPrice.text = NumberFormat.getCurrencyInstance().format(price)
        }

        fun priceColor(position: Int) {
            val color = if (position % 2 == 0) {
                ContextCompat.getColor(textPrice.context, R.color.white)
            } else {
                ContextCompat.getColor(textPrice.context, R.color.light_gray)
            }
            textPrice.setBackgroundColor(color)
        }
    }

}