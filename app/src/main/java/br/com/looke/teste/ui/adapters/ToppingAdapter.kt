package br.com.looke.teste.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.looke.teste.R
import br.com.looke.teste.domain.model.ToppingResponse
import br.com.looke.teste.extensions.setTextOrEmpty
import kotlinx.android.synthetic.main.item_topping_view.view.*

class ToppingAdapter(private val mList: List<ToppingResponse>): RecyclerView.Adapter<ToppingAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_topping_view, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val top = mList[position]
        holder.setupTexts(top.id, top.type)
    }

    override fun getItemCount() = mList.size

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val textViewId: TextView = itemView.tvId
        private val textViewType: TextView = itemView.tvType

        fun setupTexts(id: String?, type: String?) {
            textViewId.setTextOrEmpty(id)
            textViewType.setTextOrEmpty(type)
        }
    }

}