package br.com.looke.teste.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import br.com.looke.teste.R
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment: Fragment(R.layout.fragment_main) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
    }

    private fun setupListeners() {
        button_one.setOnClickListener { navigateTo(R.id.action_mainFragment_to_firstFragment) }
        button_two.setOnClickListener { navigateTo(R.id.action_mainFragment_to_secondFragment) }
        button_three.setOnClickListener { navigateTo(R.id.action_mainFragment_to_thirdFragment) }
        button_fourth.setOnClickListener { navigateTo(R.id.action_mainFragment_to_fourthFragment) }
        button_fifth.setOnClickListener { navigateTo(R.id.action_mainFragment_to_fifthFragment) }
    }

    private fun navigateTo(id: Int) {
        findNavController().navigate(id)
    }
}