package br.com.looke.teste.ui.fragments.first

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import br.com.looke.teste.R
import br.com.looke.teste.domain.model.ToppingResponse
import br.com.looke.teste.ui.adapters.ToppingAdapter
import kotlinx.android.synthetic.main.fragment_first.*
import org.koin.android.ext.android.inject

class FirstFragment : Fragment(R.layout.fragment_first) {

    private val mViewModel by inject<FirstViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerToppings()
        observerError()
        mViewModel.getList()
    }

    private fun observerToppings() {
        mViewModel.getToppingsLiveData().observe(viewLifecycleOwner, { toppings ->
            setupAdapter(toppings)
        })
    }

    private fun observerError() {
        mViewModel.getMsgErrorLiveData().observe(viewLifecycleOwner, { msg ->
            showErrorMsg(msg)
        })
    }

    private fun setupAdapter(toppings: List<ToppingResponse>) {
        val adapter = ToppingAdapter(toppings)
        rvTopping.adapter = adapter
    }

    private fun showErrorMsg(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show()
    }
}