package br.com.looke.teste.ui.fragments.first

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.looke.teste.domain.model.LookeResponse
import br.com.looke.teste.domain.model.ToppingResponse
import br.com.looke.teste.domain.remote.SafeResponse
import br.com.looke.teste.domain.remote.safeRequest
import br.com.looke.teste.domain.repository.LookeRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class FirstViewModel(
        private val mRepository: LookeRepository,
        dispatcher: CoroutineContext = Dispatchers.IO + SupervisorJob()) : ViewModel() {

    private var coroutineScope = CoroutineScope(dispatcher)
    private val mToppingsLiveData = MutableLiveData<List<ToppingResponse>>()
    private val mMsgErrorRequestLiveData = MutableLiveData<String>()

    fun getList() {
        coroutineScope.launch {

            when (val response = safeRequest { mRepository.getToppings() }) {
                is SafeResponse.Success -> handleLookeResponse(response.value)
                is SafeResponse.GenericError -> handleErrorMsg(response)
            }
        }
    }

    private fun handleLookeResponse(response: List<LookeResponse>) {
        val toppings = arrayListOf<ToppingResponse>()
        response.forEach { looke -> toppings.addAll(looke.topping) }
        setListToppingResponse(toppings)
    }

    private fun handleErrorMsg(error: SafeResponse.GenericError) {
        mMsgErrorRequestLiveData.postValue(error.errorMessage)
    }

    private fun setListToppingResponse(toppings: List<ToppingResponse>) {
        mToppingsLiveData.postValue(toppings)
    }

    fun getToppingsLiveData(): LiveData<List<ToppingResponse>> = mToppingsLiveData
    fun getMsgErrorLiveData(): LiveData<String> = mMsgErrorRequestLiveData
}