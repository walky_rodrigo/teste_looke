package br.com.looke.teste.ui.fragments.fourth

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import br.com.looke.teste.R
import br.com.looke.teste.domain.model.CarModel
import br.com.looke.teste.ui.adapters.CarAdapter
import kotlinx.android.synthetic.main.fragment_fourth.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class FourthFragment: Fragment(R.layout.fragment_fourth) {

    private val mViewModel by viewModel<FourthViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val cars = mViewModel.orderList()
        setupAdapter(cars)
    }

    private fun setupAdapter(cars: List<CarModel>) {
        val adapter = CarAdapter(cars)
        recycler_view_cars.adapter = adapter
    }
}