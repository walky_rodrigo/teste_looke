package br.com.looke.teste.ui.fragments.fourth

import androidx.lifecycle.ViewModel
import br.com.looke.teste.domain.model.CarModel

class FourthViewModel : ViewModel() {

    fun orderList(): List<CarModel> {
        return CARS_LIST.sortedBy { it.price }
    }


    companion object {
        private val CARS_LIST = arrayListOf(
                CarModel("Brasília", 8000.0),
                CarModel("Fusca", 5000.0),
                CarModel("Gol", 23000.0),
                CarModel("Kombi", 39000.0)
        )
    }

}