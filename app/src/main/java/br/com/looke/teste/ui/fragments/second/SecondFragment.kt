package br.com.looke.teste.ui.fragments.second

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import br.com.looke.teste.R
import kotlinx.android.synthetic.main.fragment_second.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SecondFragment: Fragment(R.layout.fragment_second) {

    private val mViewModel by viewModel<SecondViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
    }

    private fun setupListeners() {
        button_do_something.setOnClickListener {
            val answer = mViewModel.doSomething(NUMBER_SIX)
            showAnswer(answer.toString())
        }
    }

    private fun showAnswer(answer: String) {
        text_info.text = getString(R.string.question_two_answer, answer)
    }

    companion object {
        private const val NUMBER_SIX = 6
    }

}