package br.com.looke.teste.ui.fragments.second

import androidx.lifecycle.ViewModel

class SecondViewModel: ViewModel() {

    fun doSomething(number: Int): Int {
        return if (number <= 1) {
            number
        } else {
            (number * doSomething(number - 1))
        }
    }
}