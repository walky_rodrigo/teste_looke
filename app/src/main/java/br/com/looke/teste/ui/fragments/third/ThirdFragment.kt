package br.com.looke.teste.ui.fragments.third

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import br.com.looke.teste.R
import br.com.looke.teste.extensions.getTextInt
import kotlinx.android.synthetic.main.fragment_third.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ThirdFragment: Fragment(R.layout.fragment_third) {

    private val mViewModel by viewModel<ThirdViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
        observerAnswer()
    }

    private fun setupListeners() {
        button_calculate.setOnClickListener {
            mViewModel.calculateArrayRotation(getArraySize(), getRotate())
        }
    }

    private fun observerAnswer() {
        mViewModel.getAnswerLiveData().observe(viewLifecycleOwner, {
            showArrayAnswer(it)
        })
    }

    @SuppressLint("SetTextI18n")
    private fun showArrayAnswer(answer: String) {
        textView_answer.text = "${getString(R.string.question_three_answer)} [ $answer ]"
    }

    private fun getArraySize() = editText_array_size.getTextInt()
    private fun getRotate() = editText_rotation.getTextInt()
}