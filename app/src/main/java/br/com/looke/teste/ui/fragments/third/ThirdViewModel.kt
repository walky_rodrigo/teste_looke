package br.com.looke.teste.ui.fragments.third

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*

class ThirdViewModel: ViewModel() {

    private val mAnswerLiveData = MutableLiveData<String>()

    fun calculateArrayRotation(arraySize: Int, rotation: Int) {
        val list = calculateArrayRotation(createArray(arraySize), rotation)
        showArrayItems(list)
    }

    private fun calculateArrayRotation(array: IntArray, rotation: Int): List<Int> {
        val rotateList = array.toList()
        Collections.rotate(rotateList, -rotation)
        return rotateList
    }

    private fun showArrayItems(array: List<Int>) {
        var itens = ""
        array.forEachIndexed { index, i ->
            itens = if (index == 0) {
                "$i"
            } else {
                "$itens, $i"
            }
        }
        mAnswerLiveData.postValue(itens)
    }

    private fun createArray(arraySize: Int) = IntArray(arraySize) { it + 1}

    fun getAnswerLiveData(): LiveData<String> = mAnswerLiveData

}